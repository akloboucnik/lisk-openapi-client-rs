/*
 * Lisk API Documentation
 *
 * # Welcome!  ## Access Restrictions The API endpoints are by default restricted to a whitelist of IPs that can be found in `config.json` in the section [`api.access.whitelist`](https://github.com/LiskHQ/lisk/blob/1.0.0/config.json#L35). If you want your API to be accessable by the public, you can do this by changing `api.access.public` to `true`. This will allow anyone to make requests to your Lisk Core node. However some endpoints stay private, that means only a list of whitelisted IPs can successfully make API calls to that particular endpoint; This includes all forging related API calls. By default, only the nodes' local IP is included in the whitelist, you can change the setting in `config.json` in the section [`forging.access.whitelist`](https://github.com/LiskHQ/lisk/blob/1.0.0/config.json#L114). For more details, see the descriptions at the respective endpoint.  ## Requests Chained filter parameters are logically connected with `AND`. `HTTP` is the supported URL schema by default. To enable `HTTPS`, please adjust the the [`ssl`](https://github.com/LiskHQ/lisk/blob/1.0.0/config.json#L124) section in `config.json`.  ## Responses The general response format is JSON (`application/json`). The responses for each API request have a common basic structure: ```javascript {  \"data\": {}, //Contains the requested data  \"meta\": {}, //Contains additional metadata, e.g. the values of `limit` and `offset`  \"links\": {} //Will contain links to connected API calls from here, e.g. pagination links } ```  ## Date Formats Most of the timestamp parameters are in the Lisk Timestamp format, which is similar to the Unix Timestamp format. The **Lisk Timestamp** is the number of seconds that have elapsed since the Lisk epoch time (2016-05-24T17:00:00.000Z), not counting leap seconds. The **Lisk Epoch Time** is returned in the [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) format, combined date and time: `YYYY-MM-DDThh:mm:ssZ`. For details, see the descriptions and examples at the respective endpoint.  ## Pagination One can paginate nicely through the results by providing `limit` and `offset` parameters to the requests. `limit` and `offset` can be found in the `meta`-object of the response of an API request. If no limit and offset are provided, they are set to 10 and 0 by default, what will display the first 10 results.  ## List of Endpoints All possible API endpoints for Lisk Core are listed below. Click on an endpoint to show descriptions, details and examples. 
 *
 * The version of the OpenAPI document: 1.0.32
 * Contact: admin@lisk.io
 * Generated by: https://openapi-generator.tech
 */


use reqwest;

use crate::apis::ResponseContent;
use super::{Error, configuration};


/// struct for typed errors of method `get_transactions`
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum GetTransactionsError {
    Status400(crate::models::ParamErrorResponse),
    Status429(crate::models::RequestLimitError),
    Status500(crate::models::UnexpectedError),
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method `post_transaction`
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum PostTransactionError {
    Status400(crate::models::ParamErrorResponse),
    Status409(crate::models::ProcessingError),
    Status429(crate::models::RequestLimitError),
    UnknownValue(serde_json::Value),
}


/// Search for a specified transaction in the system. 
pub async fn get_transactions(configuration: &configuration::Configuration, id: Option<&str>, recipient_id: Option<&str>, recipient_public_key: Option<&str>, sender_id: Option<&str>, sender_public_key: Option<&str>, sender_id_or_recipient_id: Option<&str>, _type: Option<i32>, height: Option<i32>, min_amount: Option<i32>, max_amount: Option<i32>, from_timestamp: Option<i32>, to_timestamp: Option<i32>, block_id: Option<&str>, limit: Option<i32>, offset: Option<i32>, sort: Option<&str>, data: Option<&str>) -> Result<crate::models::TransactionsResponse, Error<GetTransactionsError>> {

    let local_var_client = &configuration.client;

    let local_var_uri_str = format!("{}/transactions", configuration.base_path);
    let mut local_var_req_builder = local_var_client.get(local_var_uri_str.as_str());

    if let Some(ref local_var_str) = id {
        local_var_req_builder = local_var_req_builder.query(&[("id", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = recipient_id {
        local_var_req_builder = local_var_req_builder.query(&[("recipientId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = recipient_public_key {
        local_var_req_builder = local_var_req_builder.query(&[("recipientPublicKey", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = sender_id {
        local_var_req_builder = local_var_req_builder.query(&[("senderId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = sender_public_key {
        local_var_req_builder = local_var_req_builder.query(&[("senderPublicKey", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = sender_id_or_recipient_id {
        local_var_req_builder = local_var_req_builder.query(&[("senderIdOrRecipientId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = _type {
        local_var_req_builder = local_var_req_builder.query(&[("type", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = height {
        local_var_req_builder = local_var_req_builder.query(&[("height", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = min_amount {
        local_var_req_builder = local_var_req_builder.query(&[("minAmount", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = max_amount {
        local_var_req_builder = local_var_req_builder.query(&[("maxAmount", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = from_timestamp {
        local_var_req_builder = local_var_req_builder.query(&[("fromTimestamp", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = to_timestamp {
        local_var_req_builder = local_var_req_builder.query(&[("toTimestamp", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = block_id {
        local_var_req_builder = local_var_req_builder.query(&[("blockId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = limit {
        local_var_req_builder = local_var_req_builder.query(&[("limit", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = offset {
        local_var_req_builder = local_var_req_builder.query(&[("offset", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = sort {
        local_var_req_builder = local_var_req_builder.query(&[("sort", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = data {
        local_var_req_builder = local_var_req_builder.query(&[("data", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if local_var_status.is_success() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<GetTransactionsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Submits signed transaction object for processing by the transaction pool. Transaction objects can be generated locally either by using Lisk Commander or with Lisk Elements. 
pub async fn post_transaction(configuration: &configuration::Configuration, transaction: crate::models::TransactionRequest) -> Result<crate::models::GeneralStatusResponse, Error<PostTransactionError>> {

    let local_var_client = &configuration.client;

    let local_var_uri_str = format!("{}/transactions", configuration.base_path);
    let mut local_var_req_builder = local_var_client.post(local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&transaction);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if local_var_status.is_success() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<PostTransactionError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

