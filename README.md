# Rust API client for Lisk node Open API

## Usage

see `examples/get_latest_block.rs`

## Updating

- update crate version in scripts/generate
- run `scripts/generate.sh`
- check changes, try to run example, commit

## TODO

- [ ] decide if crate version should match Lisk api version
- [ ] run `scripts/generate.sh` periodically in CI
