#!/bin/bash
curl -s "https://node.lisk.io/api/spec" -o schema/lisk-openapi-spec.json

CRATE_NAME="lisk-openapi-client"
CRATE_VERSION="0.1.0"

# TODO add minimal update
OPENAPI_GENERATOR_VERSION=5.0.0 \
RUST_POST_PROCESS_FILE="cargo fmt --" \
    openapi-generator-cli.sh generate \
    -i schema/lisk-openapi-spec.json \
    -g rust --package-name "${CRATE_NAME}" \
    --additional-properties=packageVersion=${CRATE_VERSION},httpUserAgent="${CRATE_NAME}/${CRATE_VERSION}/rust" \
    --template-dir templates/
    --enable-post-process-file
